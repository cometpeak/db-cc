#include "txn/storage.h"

bool Storage::Read(Key key, Value* result) {
  if (data_.count(key)) {
    *result = data_[key];
    return true;
  } else {
    return false;
  }
}

void Storage::Write(Key key, Value value) {
  data_[key] = value;
  timestamps_[key] = GetTime();
}

double Storage::Timestamp(Key key) {
  if (timestamps_.count(key) == 0)
    return 0;
  return timestamps_[key];
}

bool MVStorage::Read(Key key, Value* result, uint64 mvcc_txn_id,
                     const map<uint64, TxnStatus>& pg_log_snapshot) {
  if (mv_data_.count(key) == 0)
    return false;

  vector<MVTuple> *row = mv_data_[key];
  for (vector<MVTuple>::reverse_iterator rit = row->rbegin();
       rit >= row->rend(); --rit) {
    if (pg_log_snapshot.count(rit->xmin_) == 0 && rit->xmin_ < mvcc_txn_id) {
      map<uint64, TxnStatus>::const_iterator pg_e =
        pg_log_snapshot.find(rit->xmax_);

      if (rit->xmax_ == 0 || pg_e->second == ABORTED ||
          rit->xmax_ > mvcc_txn_id || pg_e->second == INCOMPLETE) {
        *result = rit->data_;
        return true;
      }
    }
  }

  return false;
}

void MVStorage::Write(Key key, Value value, uint64 mvcc_txn_id,
                      const map<uint64, TxnStatus>& pg_log_snapshot) {
  if (mv_data_.count(key) == 0) {
    vector<MVTuple> *row = new vector<MVTuple>;
    row->push_back(MVTuple(0, mvcc_txn_id, 0, value));
    mv_data_[key] = row;
    return;
  }

  vector<MVTuple> *row = mv_data_[key];
  for (vector<MVTuple>::reverse_iterator rit = row->rbegin();
       rit < row->rend(); ++rit) {
    if (pg_log_snapshot.count(rit->xmin_) == 0 && rit->xmin_ < mvcc_txn_id) {
      map<uint64, TxnStatus>::const_iterator pg_e =
        pg_log_snapshot.find(rit->xmax_);

      if (rit->xmax_ == 0 || pg_e->second == ABORTED) {
        rit->xmax_ = mvcc_txn_id;
        row->push_back(MVTuple(0, mvcc_txn_id, 0, value));
        return;
      } else if (rit->xmax_ > mvcc_txn_id && pg_e->second == INCOMPLETE) {
        return;
      }
    }
  }
}

