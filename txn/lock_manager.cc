// Author: Alexander Thomson (thomson@cs.yale.edu)
//
// Lock manager implementing deterministic two-phase locking as described in
// 'The Case for Determinism in Database Systems'.

#include "txn/lock_manager.h"
#include <cassert>

LockManagerA::LockManagerA(deque<Txn*>* ready_txns) {
  ready_txns_ = ready_txns;
}

bool LockManagerA::WriteLock(Txn* txn, const Key& key) {
  if (lock_table_.count(key) == 0) {
    lock_table_[key] = new deque<LockRequest>;
  }
  if (txn_waits_.count(txn) == 0) {
    txn_waits_[txn] = 0;
  }

  LockRequest request = LockRequest(EXCLUSIVE, txn);
  lock_table_[key]->push_back(request);
  if (lock_table_[key]->front().txn_ == txn) {
    return true;
  } else {
    txn_waits_[txn]++;
    return false;
  }
}

bool LockManagerA::ReadLock(Txn* txn, const Key& key) {
  return WriteLock(txn, key);
}

void LockManagerA::Release(Txn* txn, const Key& key) {
  if (lock_table_.count(key) == 0) {
    lock_table_[key] = new deque<LockRequest>;
  }
  if (txn_waits_.count(txn) == 0) {
    txn_waits_[txn] = 0;
  }

  vector<Txn*> owners;
  bool is_owner = false;
  Txn* last_owner = NULL;
  Status(key, &owners);
  for (vector<Txn*>::iterator it = owners.begin(); it != owners.end(); it++) {
    if (*it == txn) {
      is_owner = true;
    } else {
      last_owner = *it;
    }
  }

  deque<LockRequest> *requests = lock_table_[key];
  deque<LockRequest>::iterator next;
  for (deque<LockRequest>::iterator it = requests->begin();
       it != requests->end(); it++) {
    if (it->txn_ == txn) {
      txn_waits_[it->txn_] = 0;
      requests->erase(it);
      break;
    }
  }

  vector<Txn*> new_owners;
  Status(key, &new_owners);
  vector<Txn*>::iterator first_new_owner = new_owners.begin();
  for (vector<Txn*>::iterator it = new_owners.begin();
       it != new_owners.end(); it++) {
    if (last_owner && *it == last_owner) {
      first_new_owner = it + 1;
      break;
    }
  }
  for (vector<Txn*>::iterator it = first_new_owner;
       it != new_owners.end(); it++) {
    if (txn_waits_[*it] > 0) {
      txn_waits_[*it]--;
    }
    if (txn_waits_[*it] == 0) {
      ready_txns_->push_back(*it);
    }
  }
}

LockMode LockManagerA::Status(const Key& key, vector<Txn*>* owners) {
  if (lock_table_.count(key) == 0) {
    lock_table_[key] = new deque<LockRequest>;
  }
  deque<LockRequest>::iterator it = lock_table_[key]->begin();


  // UNLOCKED case
  if (it == lock_table_[key]->end()) {
    return UNLOCKED;
  }

  owners->resize(0);
  // EXCLUSIVE case
  if (it->mode_ == EXCLUSIVE) {
    owners->push_back(it->txn_);
    return EXCLUSIVE;
  }

  // SHARED case
  for (; it != lock_table_[key]->end(); it++) {
    if (it->mode_ != SHARED) {
      return SHARED;
    }
    owners->push_back(it->txn_);
  }

  return SHARED;
}

LockManagerB::LockManagerB(deque<Txn*>* ready_txns) {
  ready_txns_ = ready_txns;
}

bool LockManagerB::WriteLock(Txn* txn, const Key& key) {
  if (lock_table_.count(key) == 0) {
    lock_table_[key] = new deque<LockRequest>;
  }
  if (txn_waits_.count(txn) == 0) {
    txn_waits_[txn] = 0;
  }

  LockRequest request = LockRequest(EXCLUSIVE, txn);
  lock_table_[key]->push_back(request);
  if (lock_table_[key]->front().txn_ == txn) {
    return true;
  }

  txn_waits_[txn]++;
  return false;
}

bool LockManagerB::ReadLock(Txn* txn, const Key& key) {
  if (lock_table_.count(key) == 0) {
    lock_table_[key] = new deque<LockRequest>;
  }
  if (txn_waits_.count(txn) == 0) {
    txn_waits_[txn] = 0;
  }

  LockRequest request = LockRequest(SHARED, txn);
  lock_table_[key]->push_back(request);

  bool blocked = false;
  for (deque<LockRequest>::iterator it = lock_table_[key]->begin();
       it != lock_table_[key]->end(); it++) {
    if (it->mode_ == EXCLUSIVE) {
      blocked = true;
    }
  }

  if (blocked) {
    txn_waits_[txn]++;
    return false;
  } else {
    return true;
  }
}

void LockManagerB::Release(Txn* txn, const Key& key) {
  if (lock_table_.count(key) == 0) {
    lock_table_[key] = new deque<LockRequest>;
  }
  if (txn_waits_.count(txn) == 0) {
    txn_waits_[txn] = 0;
  }

  vector<Txn*> owners;
  bool is_owner = false;
  Txn* last_owner = NULL;
  Status(key, &owners);
  for (vector<Txn*>::iterator it = owners.begin(); it != owners.end(); it++) {
    if (*it == txn) {
      is_owner = true;
    } else {
      last_owner = *it;
    }
  }

  deque<LockRequest> *requests = lock_table_[key];
  deque<LockRequest>::iterator next;
  for (deque<LockRequest>::iterator it = requests->begin();
       it != requests->end(); it++) {
    if (it->txn_ == txn) {
      txn_waits_[it->txn_] = 0;
      requests->erase(it);
      break;
    }
  }

  vector<Txn*> new_owners;
  Status(key, &new_owners);
  vector<Txn*>::iterator first_new_owner = new_owners.begin();
  for (vector<Txn*>::iterator it = new_owners.begin();
       it != new_owners.end(); it++) {
    if (last_owner && *it == last_owner) {
      first_new_owner = it + 1;
      break;
    }
  }
  for (vector<Txn*>::iterator it = first_new_owner;
       it != new_owners.end(); it++) {
    if (txn_waits_[*it] > 0) {
      txn_waits_[*it]--;
    }
    if (txn_waits_[*it] == 0) {
      ready_txns_->push_back(*it);
    }
  }
}

LockMode LockManagerB::Status(const Key& key, vector<Txn*>* owners) {
  if (lock_table_.count(key) == 0) {
    lock_table_[key] = new deque<LockRequest>;
  }
  deque<LockRequest>::iterator it = lock_table_[key]->begin();


  // UNLOCKED case
  if (it == lock_table_[key]->end()) {
    return UNLOCKED;
  }

  owners->resize(0);
  // EXCLUSIVE case
  if (it->mode_ == EXCLUSIVE) {
    owners->push_back(it->txn_);
    return EXCLUSIVE;
  }

  // SHARED case
  for (; it != lock_table_[key]->end(); it++) {
    if (it->mode_ != SHARED) {
      return SHARED;
    }
    owners->push_back(it->txn_);
  }

  return SHARED;
}

